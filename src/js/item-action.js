import { getCookie, setCookie } from './cookie-storage';
import { renderItemList } from './render-item-list';

const inputFormElement = document.querySelector('.todo-app__form-input');
const buttonRemoveEl = document.querySelector('.todo-app__action-remove');

export function editItem(itemId) {
  const items = JSON.parse(getCookie('items'));
  const itemEdit = items.find((item) => {
    const result = Number.parseInt(itemId, 10) === item.id;
    return result;
  });
  inputFormElement.value = itemEdit.name;
  setCookie('editItemId', itemId);
}

export function deleteItem(itemId) {
  if (getCookie('editItemId') === itemId) return;
  const items = JSON.parse(getCookie('items'));
  const newItems = items.filter((item) => {
    const result = Number.parseInt(itemId, 10) !== item.id;
    return result;
  });
  setCookie('items', JSON.stringify(newItems));
  renderItemList(newItems);
  if (newItems.length === 0) {
    buttonRemoveEl.classList.add('disable');
  }
}
