const toDoListElement = document.querySelector('.todo-app__list');

// eslint-disable-next-line import/prefer-default-export
export function resetToDoList() {
  toDoListElement.innerHTML = '';
}
