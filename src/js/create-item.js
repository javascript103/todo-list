import { getCookie, setCookie } from './cookie-storage';

const getIncrementItemId = () => {
  let lastId = getCookie('lastId') ? getCookie('lastId') : 0;
  // eslint-disable-next-line no-plusplus
  ++lastId;
  setCookie('lastId', lastId);
  return lastId;
};

// eslint-disable-next-line import/prefer-default-export
export function createItem(itemName) {
  const newItem = {
    name: itemName,
    id: getIncrementItemId(),
  };
  return newItem;
}
