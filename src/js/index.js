import '../styles/reset.css';
import '../styles/style.scss';
import { getCookie, setCookie, deleteCookie } from './cookie-storage';
import { editItem, deleteItem } from './item-action';
import { renderItemList } from './render-item-list';
import { createItem } from './create-item';
import { resetToDoList } from './reset-list';

const inputFormElement = document.querySelector('.todo-app__form-input');
const buttonFormElement = document.querySelector('.todo-app__form-button');
const toDoListElement = document.querySelector('.todo-app__list');
const buttonRemoveEl = document.querySelector('.todo-app__action-remove');

const updateState = () => {
  if (inputFormElement.value === '') {
    return;
  }
  const editItemId = getCookie('editItemId');
  const items = getCookie('items') ? JSON.parse(getCookie('items')) : [];
  if (editItemId) {
    const itemIndex = items.findIndex((item) => {
      const result = Number.parseInt(editItemId, 10) === item.id;
      return result;
    });
    items[itemIndex].name = inputFormElement.value;
    deleteCookie('editItemId');
    setCookie('items', JSON.stringify(items));
  } else {
    const newItem = createItem(inputFormElement.value);
    items.push(newItem);
    setCookie('items', JSON.stringify(items));
  }
  renderItemList(items);
  inputFormElement.value = '';
  if (buttonRemoveEl.classList.contains('disable')) {
    buttonRemoveEl.classList.remove('disable');
  }
};

const removeToDoList = () => {
  resetToDoList();
  deleteCookie('editItemId');
  deleteCookie('items');
  deleteCookie('lastId');
  buttonRemoveEl.classList.add('disable');
};

const listClickHandler = (event) => {
  const { target } = event;
  const itemId = target.dataset.id;
  if (!itemId) {
    return;
  }
  if (target.dataset.action === 'edit') {
    editItem(itemId);
  }
  if (target.dataset.action === 'delete') {
    deleteItem(itemId);
  }
};

buttonFormElement.addEventListener('click', updateState);
buttonRemoveEl.addEventListener('click', removeToDoList);
toDoListElement.addEventListener('click', listClickHandler);

(function () {
  const items = getCookie('items') ? JSON.parse(getCookie('items')) : [];
  if (items.length) {
    renderItemList(items);
  }
  if (items.length !== 0) {
    buttonRemoveEl.classList.remove('disable');
  }
}());
