import { createHTML } from './create-html';
import { resetToDoList } from './reset-list';

const toDoListElement = document.querySelector('.todo-app__list');

// eslint-disable-next-line import/prefer-default-export
export function renderItemList(items) {
  resetToDoList();
  items.forEach((item) => {
    const itemElement = createHTML(item);
    toDoListElement.append(itemElement);
  });
}
