// const inputFormElement = document.querySelector('.todo-app__form-input');
// const buttonFormElement = document.querySelector('.todo-app__form-button');
// const toDoListElement = document.querySelector('.todo-app__list');
// const buttonRemoveEl = document.querySelector('.todo-app__action-remove');

const createActionItemEl = (id) => {
  const actionItemEl = document.createElement('span');
  actionItemEl.classList.add('todo-app__list-item-action');

  const buttonEditEl = document.createElement('button');
  buttonEditEl.setAttribute('data-action', 'edit');
  buttonEditEl.setAttribute('data-id', id);
  buttonEditEl.classList.add('todo-app__list-item-action-button');

  const buttonDeleteEl = document.createElement('button');
  buttonDeleteEl.setAttribute('data-action', 'delete');
  buttonDeleteEl.setAttribute('data-id', id);
  buttonDeleteEl.classList.add('todo-app__list-item-action-button');

  actionItemEl.append(buttonEditEl);
  actionItemEl.append(buttonDeleteEl);

  return actionItemEl;
};

// eslint-disable-next-line import/prefer-default-export
export function createHTML(item) {
  const itemElement = document.createElement('li');
  itemElement.classList.add('todo-app__list-item');
  const itemNameElement = document.createElement('span');
  itemNameElement.textContent = item.name;
  itemNameElement.classList.add('todo-app__list-item-name');
  itemElement.append(itemNameElement);

  const actionItemEl = createActionItemEl(item.id);
  itemElement.append(actionItemEl);

  return itemElement;
}
